#### *Purpose*: Identify direct and indirect causes of a target (sink node) time series, given a pool of candidate time series given some graph constraints.

#### *Content*: Code (SyPI algorithm) for ICML 2021 paper: "Necessary and sufficient conditions for causal feature selection on time series with latent common causes". This code was developed during my internship in Amazon as well a during my PhD in Max PLanck Institute for Intelligent Systems afterwards.
When using the code please cite:

    @inproceedings{mastakouri2021necessary,
    title={Necessary and sufficient conditions for causal feature selection in time series with latent common causes},
    author={Mastakouri, Atalanti A and Sch{\"o}lkopf, Bernhard and Janzing, Dominik},
	booktitle={International Conference on Machine Learning},
    pages={7502--7511},
    year={2021},
    organization={PMLR}
    }


####					                    - SyPI_method.py

    INPUT: regression_algorithm, relationships, normalized_data, normalization, lambda_reg, p_cond1, p_cond2, threshold_lasso, var_direct_causes, var_indirect_causes, var_non_causes,
	X (time series with the time steps on the columns. Last row the target time series of interest for which we want to identify causes)
    OUTPUT: predicted_causes, predicted_non_causes, false_positives_predict_direct_and_indirect, true_positives_predict_direct_and_indirect, false_negatives_predict_direct_and_indirect,
	true_negatives_predict_direct_and_indirect, n_vars, strength_of_predicted_causes










